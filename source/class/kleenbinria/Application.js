/* ************************************************************************

   Copyright:

   License:

   Authors:

 ************************************************************************ */

/* ************************************************************************

 #asset(kleenbinria/*)

 ************************************************************************ */

/**
 * This is the main application class of your custom application "KleenbinRIA"
 */
qx.Class.define("kleenbinria.Application", {
    extend : qx.application.Standalone,

    /*
     * ****************************************************************************
     * MEMBERS
     * ****************************************************************************
     */

    members : {
        
        //private model members
        __business: null,
        
        initialiseModel: function() {
          
            this.__business = qx.data.marshal.Json.createModel({numberContracts:"0", franchiseArea:"xyz", numberBins:"0"});
        },
        
        //private screen members
        __customerForm: null,
        __mainTopContainer: null,
        __mainLeftContainer: null,
        
        buildMainLeftContainer: function() {

            if(this.__mainLeftContainer == null) {
                this.__mainLeftContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox());

                var paymentsContainer = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
                
                var paymentHistoryGrid = new qx.ui.container.Composite(new qx.ui.layout.Grid());
                paymentHistoryGrid.add(new qx.ui.basic.Label("Reference"), {row: 0, column: 0});
                paymentHistoryGrid.add(new qx.ui.basic.Label("Amount"), {row: 0, column: 1});
                paymentHistoryGrid.add(new qx.ui.basic.Label("Date"), {row: 0, column: 2});
                
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 1, column: 0});
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 1, column: 1});
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 1, column: 2});
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 2, column: 0});
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 2, column: 1});
                paymentHistoryGrid.add(new qx.ui.form.TextField(), {row: 2, column: 2});
                paymentsContainer.add(paymentHistoryGrid, {left: 10, top: 10});
                
                var grid = new qx.ui.layout.Grid();
                grid.setColumnAlign(1, "center", "bottom");
                var dueGrid = new qx.ui.container.Composite(grid);
                dueGrid.add(new qx.ui.basic.Label("B/BF (Last Year)"), {row: 0, column: 0});
                dueGrid.add(new qx.ui.basic.Label("R 0.00"), {row: 0, column: 1});
                dueGrid.add(new qx.ui.basic.Label("Invoiced"), {row: 1, column: 0});
                dueGrid.add(new qx.ui.basic.Label("R 52.00"), {row: 1, column: 1});
                dueGrid.add(new qx.ui.basic.Label("Receipts"), {row: 2, column: 0});
                dueGrid.add(new qx.ui.basic.Label("R 0.00"), {row: 2, column: 1});
                dueGrid.add(new qx.ui.basic.Label("Balance"), {row: 3, column: 0});
                dueGrid.add(new qx.ui.basic.Label("R 52.00"), {row: 3, column: 1});
                dueGrid.add(new qx.ui.basic.Label("Months Overdue"), {row: 4, column: 0});
                dueGrid.add(new qx.ui.basic.Label("1"), {row: 4, column: 1});
                
                paymentsContainer.add(dueGrid, {left: 350, top:10});
                
                this.__mainLeftContainer.add(paymentsContainer, {flex: 1});
                
                this.__mainLeftContainer.add(new qx.ui.basic.Label("for invoices"), {flex: 1});
            }
        },
        
        buildCustomerForm : function() {
            if(this.__customerForm == null) {
                this.__customerForm = new qx.ui.form.Form();
                this.__customerForm.add(new qx.ui.form.TextField(), "Contract no");
                this.__customerForm.add(new qx.ui.form.TextField(), "Joining Month");
                this.__customerForm.add(new qx.ui.form.TextField(), "Surname/Company");
                this.__customerForm.add(new qx.ui.form.TextField(), "Title");
                this.__customerForm.add(new qx.ui.form.TextField(), "Initials");
                this.__customerForm.add(new qx.ui.form.TextField(), "First Name");
                this.__customerForm.add(new qx.ui.form.TextField(), "No");
                this.__customerForm.add(new qx.ui.form.TextField(), "Street");
                this.__customerForm.add(new qx.ui.form.TextField(), "Suburb");
                this.__customerForm.add(new qx.ui.form.TextField(), "Postal Code");
                this.__customerForm.add(new qx.ui.form.TextField(), "Sales Consultant");
                this.__customerForm.add(new qx.ui.form.TextField(), "Day Of Cleaning");
                this.__customerForm.add(new qx.ui.form.TextField(), "Trailer Allocation");
                this.__customerForm.add(new qx.ui.form.TextField(), "Frequency");
                this.__customerForm.add(new qx.ui.form.TextField(), "No Of Bins");
                this.__customerForm.add(new qx.ui.form.TextField(), "Home Tel No");
                this.__customerForm.add(new qx.ui.form.TextField(), "Work Tel No");
                this.__customerForm.add(new qx.ui.form.TextField(), "Cell No");
                this.__customerForm.add(new qx.ui.form.TextField(), "Start Date");
                this.__customerForm.add(new qx.ui.form.TextField(), "Payment name");
                this.__customerForm.add(new qx.ui.form.TextField(), "Company Vat No");
            }
        },
        
        buildMainTopContainer : function() {
            
            if(this.__mainTopContainer == null) {
                //add main top container
                this.__mainTopContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox());
                
                
                var leftHeader = new qx.ui.container.Composite(new qx.ui.layout.HBox().set({spacing:10, alignY:"bottom"}));
                
//                var label = new qx.ui.basic.Label("Contracts");
//                label.setFont("");
                
//                leftHeader.add(label);
                leftHeader.add(new qx.ui.basic.Label("Contracts").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({italic:true, bold:true}), 
                            textColor:"blue"}
                ));
                var numberContractsLabel = new qx.ui.basic.Label("???").set(
                        {
                            font : new qx.bom.Font(20, ["Verdana", "sans-serif"]).set({bold:true}),
                            textColor:"red"}
                );
                leftHeader.add(numberContractsLabel);
                
                var rightHeader = new qx.ui.container.Composite(new qx.ui.layout.HBox().set({spacing:10, alignY:"bottom"}));
                rightHeader .add(new qx.ui.basic.Label("Bins").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({italic:true, bold:true}), 
                            textColor:"blue"}
                ));
                var numberBinsLabel = new qx.ui.basic.Label("???").set(
                        {
                            font : new qx.bom.Font(20, ["Verdana", "sans-serif"]).set({bold: true}),
                            textColor:"red"}
                );
                rightHeader.add(numberBinsLabel);
                
                var infoHeader = new qx.ui.container.Composite(new qx.ui.layout.Canvas());
                infoHeader.add(leftHeader, {left: "0%"});
                var franchiseAreaLabel = new qx.ui.basic.Label("???").set(
                        {
                            font : new qx.bom.Font(20, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"red"}
                );
                infoHeader.add(franchiseAreaLabel, {left: "50%"});
                infoHeader.add(rightHeader, {left: "90%"});
                
                this.__business.addListener("changeFranchiseArea", function(e) {
                    var model = e.getTarget();
                    this.setValue("" + model.getFranchiseArea());
                  }, franchiseAreaLabel);
                
                this.__business.addListener("changeNumberContracts", function(e) {
                    var model = e.getTarget();
                    this.setValue("" + model.getNumberContracts());
                }, numberContractsLabel);
                
                this.__business.addListener("changeNumberBins", function(e) {
                    var model = e.getTarget();
                    this.setValue("" + model.getNumberBins());
                }, numberBinsLabel);
                
                this.__mainTopContainer.add(infoHeader);
                
                var grid = new qx.ui.layout.Grid();
                grid.setRowAlign(0, "center", "bottom");
                var searchContainer = new qx.ui.container.Composite(grid);

                searchContainer.add(new qx.ui.basic.Label("Surname").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:0});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:0});
                
                searchContainer.add(new qx.ui.basic.Label("Account").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:1});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:1});
                
                searchContainer.add(new qx.ui.basic.Label("Street No").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:2});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:2});
                
                searchContainer.add(new qx.ui.basic.Label("Street").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:3});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:3});
                
                searchContainer.add(new qx.ui.basic.Label("Payment Name").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:4});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:4});
                
                searchContainer.add(new qx.ui.basic.Label("H Tel").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:5});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:5});
                
                searchContainer.add(new qx.ui.basic.Label("W Tel").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:6});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:6});
                
                searchContainer.add(new qx.ui.basic.Label("Cell").set(
                        {
                            font : new qx.bom.Font(10, ["Verdana", "sans-serif"]).set({bold:true}), 
                            textColor:"blue"}
                ), {row: 0, column:7});
                searchContainer.add(new qx.ui.form.TextField(), {row: 1, column:7});
                
                this.__mainTopContainer.add(searchContainer);
                
                var customerInforContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox().set({alignX:"center",spacing:40}));
                
                customerInforContainer.add(new qx.ui.basic.Label("Account No. 236"));
                customerInforContainer.add(new qx.ui.basic.Label("Mr M.P Melene"));
                customerInforContainer.add(new qx.ui.basic.Label("10 Gabriel Dijkman Street"));
                customerInforContainer.add(new qx.ui.basic.Label("The Orchards"));
                customerInforContainer.add(new qx.ui.basic.Label("1"));
                
                this.__mainTopContainer.add(customerInforContainer);
            }
        },
        
        /**
         * This method contains the initial application code and gets called
         * during startup of the application
         * 
         * @lint ignoreDeprecated(alert)
         */
        main : function() {
            // Call super class
            this.base(arguments);

            // Enable logging in debug variant
            if (qx.core.Environment.get("qx.debug")) {
                // support native logging capabilities, e.g. Firebug for Firefox
                qx.log.appender.Native;
                // support additional cross-browser console. Press F7 to toggle
                // visibility
                qx.log.appender.Console;
            }

            this.initialiseModel();
            
            //add inner container
            var innerVContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox());
            
            //add centering dock
            var centeringLayout = new qx.ui.layout.Dock();
            var centeringContainer = new qx.ui.container.Composite(centeringLayout);
            
            centeringContainer.add(innerVContainer);
            
            var scroll = new qx.ui.container.Scroll();
            scroll.add(centeringContainer);
            
            var container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.VBox();
            container.setLayout(layout);

            container.add(scroll, {flex: 1});

            this.getRoot().add(container, {edge: 0});
            
            var doc = this.getRoot();
            doc.add(scroll, {edge: 0});
            
            //smaller bits and pieces            
            this.buildMainTopContainer();
            innerVContainer.add(this.__mainTopContainer);
            
            //add main H-Box
            var mainHLayout = new qx.ui.layout.HBox();
            var mainHContainer = new qx.ui.container.Composite(mainHLayout);
            
            this.buildCustomerForm();
            var single = new qx.ui.form.renderer.Single(this.__customerForm);
            single.getLayout().setSpacingY(0);
            mainHContainer.add(single);
            
            this.buildMainLeftContainer();
            mainHContainer.add(this.__mainLeftContainer, {flex: 1});
            
            innerVContainer.add(mainHContainer);
            
            /*
             * set up initial load request
             */

            var initialLoadRequest = new qx.io.request.Xhr();
            initialLoadRequest.setUrl("http://localhost:8081/kleenbinapp/business.action");
            initialLoadRequest.setMethod("POST");
            initialLoadRequest.setRequestData({"viewContext":"json"});
            
            initialLoadRequest.addListener("success", function(e) {
                var req = e.getTarget();
                var response = req.getResponseText();
                var business = JSON.parse(response);
                this.__business.setNumberContracts(business.numberContracts);
                this.__business.setFranchiseArea(business.franchiseArea);
                this.__business.setNumberBins(business.numberBins);
              }, this);
            
            initialLoadRequest.send();
        }
    }
});
