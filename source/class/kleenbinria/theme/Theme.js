/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("kleenbinria.theme.Theme",
{
  meta :
  {
    color : kleenbinria.theme.Color,
    decoration : kleenbinria.theme.Decoration,
    font : kleenbinria.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : kleenbinria.theme.Appearance
  }
});